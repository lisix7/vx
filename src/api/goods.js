import request from '@/utils/request'

export function gitList(query) {
  return request({
    url: '/goods/list',
    method: 'get',
    params: query
  })
}
